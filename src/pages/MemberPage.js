import React , { useState } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
 
import MemberAdd from '../components/MemberAdd';
import MemberList from '../components/MemberList';
 
import {
        Section,
        Heading,
        Columns
} from 'react-bulma-components';
 
const MemberPage = (props) => {
        /* 1.) Create state (i.e., Single Source of Truth)  */
        const[members,setMembers] = useState([]);
 
        /* 2.) Create a new function to add a document (e.g add Member)
                - we can pass this as props to our child add Member component
        */
        const addMember = (newMember) => {
                setMembers([...members,newMember]);
        }
        const sectionStyle = {
                paddingTop: "3em",
                paddingBottom: '3em'
        }
 
 
        return (
                <Section size="medium" style={ sectionStyle }>
                   <Heading>Members</Heading>
                   <Columns>
                         <Columns.Column size={4}>
                              {/* 3) Pass the new function as prop to the child component  */}
                              <MemberAdd addMember={addMember} />
                            </Columns.Column>
 
                            <Columns.Column>
                              {/* 6) pass props to second child  */}
                              <MemberList members={members} />
                            </Columns.Column>                        
                   </Columns>
                </Section>
            )
       
}
 
export default MemberPage;

