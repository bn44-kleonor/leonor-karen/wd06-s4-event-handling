import React, { useState } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';

import TaskAdd from '../components/TaskAdd';
import TaskList from '../components/TaskList';

import {
	Section,
	Heading,
	Columns
} from 'react-bulma-components';

const TaskPage = (props) => {
	/* 1.) Create state (i.e., Single Source of TRUTH */ 
	const[tasks, setTasks] = useState([]);

	/* 2.) Create a new function to add a document (e.g. add Task) 
		- we can pass this as props to our child add Task component
	*/ 
	const addTask = (newTask) => {
		setTasks([...tasks,newTask]);
	}

	const sectionStyle = {
		paddingTop: "3em",
		paddingBottom: '3em'
	};

	return (
			<Section size="medium" style={ sectionStyle }>
				<Heading>Tasks</Heading>
				<Columns>
					<Columns.Column size={4}>
						{/* 3) Pass the new function as prop to the child component*/}
						<TaskAdd addTask={addTask} />
					</Columns.Column>

					<Columns.Column>
						{/* 6) Pass props to second child*/}
						<TaskList tasks={tasks}/>
					</Columns.Column>
				</Columns>
			</Section>
	)
}

export default TaskPage;
