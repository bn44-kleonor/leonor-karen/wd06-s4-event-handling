import React, { useState } from 'react';

const TaskRow = (props) => {
	const task = props.task;
	return (
		<tr key={task._id}>
			<td>{task.name}</td>
			<td>{task.assignedTeam}</td>
			<td>{task.action}</td>
		</tr>
	)
}
export default TaskRow;

