import React, { useState } from 'react';
//10) importing sweetalert2
import Swal from 'sweetalert2';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';

const TeamAdd = (props) => {
	/*4. Go to child component (e.g. TeamAdd)
	Add state to child component.
	 - state variables will be bound to individual input fields
	*/
	const [name, setName] = useState("");


	/*
		b. create addTeam function
		- prevent default behavior of form
		- reset values of input fields
		- call props from parent component to create a new document
	*/

	const addTeam = (e) => {
		e.preventDefault();

		const newTeam = {
			name: name
		}
	
		setName("");

		props.addTeam(newTeam);
		//11) fire sweetAlert
		Swal.fire({
			title: "Team Added",
			text: "Team has been added",
			type: "success"
		})
	}


		return (
			<Card>
				<Card.Header>
					<Card.Header.Title>
						Add Team
					</Card.Header.Title>
				</Card.Header>
				<Card.Content>
					{/*
					5. Employ two-way binding
						- bind every input element to its respective setState variable
						- bind an onChange event to change the state as the input value changes
					*/}


					<form onSubmit={addTeam}>
						<div className="field">
							<label className="label">Name</label>
							<div className="control">
								<input 
								className="input" 
								type="text" 
								required
								value={name}
								onChange={e => setName(e.target.value)}/>
							</div>
						</div>
						<div className="field">
							<div className="control">
								<button className="button is-link is-success">Add</button>
							</div>
						</div>
					</form>
				</Card.Content>
			</Card>
		)
}

export default TeamAdd;