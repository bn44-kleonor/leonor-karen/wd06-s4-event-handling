import React, { useState, useEffect } from 'react';
// 8. Import React's prop-types
import PropTypes from 'prop-types';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';
import TaskRow from './TaskRow';

const TaskList = (props) => {

	let rows = "";
	if(props.tasks.length === 0 ){
		rows = (
				<tr>
					<td colSpan="4">
						<em>No Tasks Found</em>
					</td>
				</tr>
			)
	} else {
		rows = props.tasks.map(task=>{
			return <TaskRow task={task}/>
		})
	}

	return (
			<Card>
				<Card.Header>
					<Card.Header.Title>
						Task List
					</Card.Header.Title>
				</Card.Header>
				<Card.Content>
					<table className="table is-fullwidth is-bordered is-striped is-hoverable">
						<thead>
							<tr>
								<th>Task Description</th>
								<th>Assigned Team</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							{rows}
						</tbody>
					</table>
				</Card.Content>
			</Card>
		)
}

//9. Apply prop type checking in component
TaskList.propTypes = {
	tasks:PropTypes.array
}

export default TaskList;