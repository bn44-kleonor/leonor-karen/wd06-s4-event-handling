import React, { useState,  } from 'react';
//10) importing sweetalert2
import Swal from 'sweetalert2';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
		Card
} from 'react-bulma-components';

const TaskAdd = (props) => {

	/*4. Go to a child component (e.g. TaskAdd)
	Add state to child component.
	 - state variables will be bound to individual input fields
	*/
	const [name, setName] = useState("");
	const [teamId, setTeamId] = useState("undefined");

	/*
	 $.b. create addTask function
	- prevent default behavior of form
	- reset values of input fields
	- call props from parent component to create a new document
	*/
	const addTask = (e) => {
		e.preventDefault();

		const newTask = {
			name: name,
			teamId: teamId
		}

		setName("");
		setTeamId("undefined");

		props.addTask(newTask);
		//11) fire sweetAlert
		Swal.fire({
			title: "Task Added",
			text: "Task has beed added!",
			type: "success"
		})
	}

		return (
			<Card>
				<Card.Header>
					<Card.Header.Title>
						Add Task
					</Card.Header.Title>
				</Card.Header>
				<Card.Content>
				{/*
					5. Employ two-way binding
						- bind every input element to its respective setState variable
						- bind an onChange event to change the state as the input value changes
0				*/}
					<form onSubmit={addTask}>
						<div className="field">
							<label className="label">Task Description</label>
							<div className="control">
								<input 
								className="input"
								type="text"
								required
								value={name}
								onChange={e => setName(e.target.value)}
								/>
							</div>
						</div>
						<div className="field">
						  <label className="label">Team</label>
						  <div className="control">
						    <div className="select is-fullwidth">
						      <select
						      	defaultValue={"DEFAULT"}
						      		required
						      		value={teamId}
						      		onChange={e => setTeamId(e.target.value)}
						      >
	                              <option value="DEFAULT" disabled>Select team</option>
	                              <option value="Instructors">Instructors Team</option>
	                              <option value="HR">HR Team</option>
	                              <option value="Admin">Admin Team</option>
	                              <option value="Tech">Tech Team</option>
						      </select>
						    </div>
						  </div>
						</div>
						<div className="field">
							<div className="control">
								<button className="button is-link is-success">Add</button>
							</div>
						</div>
					</form>
				</Card.Content>
			</Card>
		)
}

export default TaskAdd;