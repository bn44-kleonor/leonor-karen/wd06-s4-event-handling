import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from "react-router-dom";

import MemberPage from './pages/MemberPage';
import TeamPage from './pages/TeamPage';
import TaskPage from './pages/TaskPage';
import NotFound from './pages/NotFound';
import AppNavbar from './components/AppNavbar';

const root = document.querySelector("#root");

const pageComponent = (
	<BrowserRouter>
		<AppNavbar />
		<Switch>

			<Route exact path="/members" component={MemberPage} />

			<Route exact path="/tasks" component={TaskPage} />
			
			<Route exact path="/teams" component={TeamPage} />
			
			<Route component={NotFound} />
		
		</Switch>
	</BrowserRouter>
);

ReactDOM.render(pageComponent, root);
